package com.zenity.appium.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;;import java.time.Duration;

public abstract class AbstractPage {

    protected final AppiumDriver appiumDriver;

    protected AbstractPage(AppiumDriver appiumDriver) {
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver, Duration.ofSeconds(5)), this);
        this.appiumDriver = appiumDriver;
    }
}
