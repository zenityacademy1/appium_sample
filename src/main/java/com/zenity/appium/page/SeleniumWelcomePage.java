package com.zenity.appium.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class SeleniumWelcomePage extends AbstractPage {

    private final String url = "https://www.selenium.dev";
    private final String title = "Selenium";

    // defining this page elements and how to locate them
    @AndroidFindBy(xpath = "//button[@aria-label='Toggle navigation']")
    private WebElement hamburgerMenu;

    public SeleniumWelcomePage(AppiumDriver driver) {
        super(driver);
    }

    public String getTitle() {
        return this.title;
    }

    public void access() {
        appiumDriver.get(url);
    }

    public SeleniumDocumentationPage clickOnInsideZenityLink() {
        hamburgerMenu.click();
        return new SeleniumDocumentationPage(appiumDriver);
    }

}
