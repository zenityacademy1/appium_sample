package com.zenity.appium.page;

import io.appium.java_client.AppiumDriver;

public class SeleniumDocumentationPage extends AbstractPage {

    protected SeleniumDocumentationPage(AppiumDriver driver) {
        super(driver);
        // printing source page
        System.out.println(appiumDriver.getPageSource());
    }
}
