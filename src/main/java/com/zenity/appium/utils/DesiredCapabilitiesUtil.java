package com.zenity.appium.utils;

import com.zenity.appium.vo.AndroidEmulator;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesUtil {

    private static DesiredCapabilities getAndroidDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        return capabilities;
    }

    public static DesiredCapabilities getAndroidDesiredCapabilities(AndroidEmulator emulator) {
        DesiredCapabilities capabilities = getAndroidDesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, emulator.deviceName());
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, emulator.androidVersion());
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
        return capabilities;
    }

}
