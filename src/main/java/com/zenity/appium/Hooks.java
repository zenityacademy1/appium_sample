package com.zenity.appium;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.net.URL;

public class Hooks {
    private static AppiumDriverLocalService localAppiumServer;
    private static final Logger log = LogManager.getLogger(Hooks.class);

    @BeforeAll
    public static void beforeAll() {
        log.info("Start local Appium server");
        AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder()
                .usingAnyFreePort() // using any free port on the machine
                .withArgument(() -> "--allow-insecure","chromedriver_autodownload");
        localAppiumServer = AppiumDriverLocalService.buildService(serviceBuilder);
        localAppiumServer.start();

        log.info(String.format("Appium server started on url: '%s'%n", localAppiumServer.getUrl().toString()));
    }

    @AfterAll
    public static void afterAll() {
        if (localAppiumServer != null) {
            log.info(String.format("Stopping the local Appium server running on: '%s'%n",
                    localAppiumServer.getUrl().toString()));
            localAppiumServer.stop();
        }
    }

    public static URL getAppiumServerUrl() {
        return localAppiumServer.getUrl();
    }
}
