package com.zenity.appium.vo;

public enum AndroidEmulator {

    Pixel_2_API_28("emulator-5554", "9");

    private final String deviceName;
    private final String androidVersion;

    AndroidEmulator(String deviceName, String androidVersion) {
        this.deviceName = deviceName;
        this.androidVersion = androidVersion;
    }

    public String deviceName() {
        return this.deviceName;
    }

    public String androidVersion() {
        return this.androidVersion;
    }
}
