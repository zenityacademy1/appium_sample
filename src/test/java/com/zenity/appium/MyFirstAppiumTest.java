package com.zenity.appium;

import com.zenity.appium.page.SeleniumWelcomePage;
import com.zenity.appium.utils.DesiredCapabilitiesUtil;
import com.zenity.appium.vo.AndroidEmulator;
import io.appium.java_client.AppiumDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

// extending Hooks class is allowing us to use BeforeAll and AfterAll hooks
public class MyFirstAppiumTest extends Hooks {

    private static final Logger log = LogManager.getLogger(MyFirstAppiumTest.class);
    private static AppiumDriver appiumDriver;

    @BeforeAll
    public static void initAndroidAppiumDriver() {
        DesiredCapabilities capabilities = DesiredCapabilitiesUtil.getAndroidDesiredCapabilities(AndroidEmulator.Pixel_2_API_28);
        appiumDriver = new AppiumDriver(getAppiumServerUrl(), capabilities);
    }

    @AfterAll
    public static void tearDown() {
        appiumDriver.quit();
    }

    @Test
    public void testZenityWebsite() {
        SeleniumWelcomePage seleniumWelcomePage = new SeleniumWelcomePage(appiumDriver);
        seleniumWelcomePage.access();
        Assertions.assertEquals(seleniumWelcomePage.getTitle(), appiumDriver.getTitle());
        seleniumWelcomePage.clickOnInsideZenityLink();
        //ZenityInsideZenityPage zenityInsideZenityPage = appiumWelcomePage.clickOnInsideZenityLink();
    }

}
